[CmdletBinding()]
param(
  [Parameter(Mandatory = $true)][string]$Cert,
  [Parameter(Mandatory = $true)][string]$Key,
  [Parameter(Mandatory = $true)][string]$Password,
  [Parameter(Mandatory = $true)][string]$Pfx
)

if (Test-Path $Pfx) {
  # Get thumbprint of pem cert 
  $tempPfx = (New-TemporaryFile |  Rename-Item -NewName { $_ -replace 'tmp$', 'pfx' } -PassThru).FullName
  $pemThumbprint = (& "$PSScriptRoot\Convert-PemToPfx.ps1" -Cert "${Cert}" -Key "${Key}" -Pfx "${tempPfx}" -Password "${Password}")
  Remove-Item $tempPfx -Force

  # Get thumbprint of pfx cert
  $existingPfx = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2
  $securePassword = ConvertTo-SecureString $Password -asplaintext -force
  $existingPfx.Import((Resolve-Path $Pfx), $securePassword, [System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]::DefaultKeySet)
  $pfxThumbprint = $existingPfx.Thumbprint
}
else {
  exit 1
}
if ($pemThumbprint -eq $pfxThumbprint) {
  exit 0
}
else {
  exit 1
}
