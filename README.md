# pemtopfx

#### Table of Contents

1. [Description](#description)
2. [Usage - Configuration options and additional functionality](#usage)
3. [Reference](#reference)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)
6. [Licenses](#licenses)

## Description

Defined type `pemtopfx` that creates pfx (PKCS12) cert files in Windows from Puppet and Hiera strings containing pem certs and keys.

## Usage

To create a certificate:

```puppet
pemtopfx {'C:\ProgramData\cert.pfx':
  cert     => $pemCertString,
  key      => $pemKeyString,
  password => 'my_password',
}
```

## Reference

For detailed information, see [REFERENCE.md](https://gitlab.com/harbottle/puppet-pemtopfx/blob/master/REFERENCE.md).

## Limitations

- Windows only.

## Development

Please send pull requests.

## Licenses
**puppet-pemtopfx**: Copyright (c) 2020 Richard Grainger,
[Apache-2.0 Licence](https://www.apache.org/licenses/LICENSE-2.0)

**crypto libraries**: Copyright (c) 2011 Mario Majčica,
[CPOL Licence](http://www.codeproject.com/info/cpol10.aspx) and
Copyright (c) 2018 Československá obchodní banka,
[GPL v3 License](https://www.gnu.org/licenses/gpl-3.0.txt)