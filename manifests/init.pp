# @summary Create pfx cert files in Windows from Puppet and Hiera strings containing pem certs and keys
#
# Defined type to create pfx cert files in Windows from Puppet and Hiera strings containing pem certs and keys.
#
# @example
#   pemtopfx {'C:\ProgramData\cert.pfx':
#     cert     => $pemCertString,
#     key      => $pemKeyString,
#     password => 'my_password',
#   }
#
# @param cert
#   String containing the pem cert to convert to pfx
# @param key
#   String containing the pem key for the cert to convert to pfx
# @param password
#   Password to use when creating the pfx file
# @param pfx
#   Path of the pfx file to create
# @param scripts_dir
#   Path to store PowerShell scripts used during conversion process
define pemtopfx (
  String[1] $cert,
  String[1] $key,
  String[1] $password,
  Stdlib::Windowspath $pfx = $title,
  Stdlib::Windowspath $scripts_dir = 'C:\ProgramData',
) {
  ensure_resources('file', {
    "${scripts_dir}\\Convert-PemToPfx.ps1" => {
      ensure => present,
      source => 'puppet:///modules/pemtopfx/Convert-PemToPfx.ps1',
      mode   => '0600',
    },
    "${scripts_dir}\\Compare-PemToPfx.ps1" => {
      ensure => present,
      source => 'puppet:///modules/pemtopfx/Compare-PemToPfx.ps1',
      mode   => '0600',
    },
  })
  exec { "pemtopfx-${name}":
    provider  => powershell,
    command   => "${scripts_dir}\\Convert-PemToPfx.ps1 -Cert '${cert}' -Key '${key}' -Password '${password}' -Pfx '${pfx}'",
    unless    => "${scripts_dir}\\Compare-PemToPfx.ps1 -Cert '${cert}' -Key '${key}' -Password '${password}' -Pfx '${pfx}'",
    logoutput => true,
    require   => [
      File["${scripts_dir}\\Convert-PemToPfx.ps1"],
      File["${scripts_dir}\\Compare-PemToPfx.ps1"]
    ],
  }
}
