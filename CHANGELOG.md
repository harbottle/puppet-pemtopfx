# Changelog

All notable changes to this project will be documented in this file.

## Release 0.1.2

**Features**

**Bugfixes**

- Make cert/key decoding more reliable

**Known Issues**

## Release 0.1.1

**Features**

**Bugfixes**

- Fix PowerShell 6

**Known Issues**

## Release 0.1.0

**Features**

Initial release

**Bugfixes**

**Known Issues**
